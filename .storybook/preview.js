import { ThemeProvider } from 'styled-components';
import theme from '../src/styles/theme.js'
import { GlobalsStyles } from '../src/styles/globals-styles.js'

/** @type { import('@storybook/react').Preview } */
export const parameters = {
  backgrounds: {
    default: 'light',
    values: [
      {
        name: 'light',
        value: theme.colorsBg.mainBg,
      },
      {
        name: 'dark',
        value: theme.colorsBg.secondBg,
      },
    ]
  },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/i,
    },
  },
};

export const decorator = [
  (Story) => (
    <ThemeProvider theme={theme}>
      <Story />
      <GlobalsStyles />
    </ThemeProvider>
  )
]

