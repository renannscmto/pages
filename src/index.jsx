import React from 'react';
import ReactDOM from 'react-dom/client';

import App from '../src/templates/App/App';
import { ThemeProvider } from 'styled-components';
import GlobalStyles from "./styles/globals-styles.js";
import theme from './styles/theme.js';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
      <GlobalStyles />
    </ThemeProvider>
  </React.StrictMode>,
);
