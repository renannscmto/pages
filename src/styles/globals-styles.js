import { createGlobalStyle, css } from "styled-components";

const GlobalStyles = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap')
    
    * {
        margin: 0;
        padding: 0;
    }
    
    html {
        font-size: 62.5%
    }

    body {
        font-size: 1.6rem;
        ${({ theme }) => css`
            font-family: ${theme.font.family.default}
        `}
    }

    h1 {
        ${({ theme }) => css`
            font-family: ${theme.font.family.default}
        `};
    }
`
export default GlobalStyles