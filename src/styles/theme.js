const theme = {
    colorsBg: {
        mainBg: 'white',
        secondBg: '#0A1128',
    },

    colorsFont: {
        mainColor: 'white',
        secondColor: '#0A1128'
    },

    font: {
        family: {
            default: "'Ubuntu Condensed', sans-serif"
        }
    },

    spacings: {}
}

export default theme;