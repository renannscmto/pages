import * as Styled from './styles';
import { Heading } from '../../components/Heading';

function App() {
  return (
    <div className="App">
      <Styled.Wrapper>
        <Heading>Texto do Heading</Heading>
      </Styled.Wrapper>
    </div>
  );
}

export default App;
