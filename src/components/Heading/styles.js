import styled, { css } from "styled-components";

export const Title = styled.h1`
    ${({ theme, ligth }) => css`
        color: ${ligth ? theme.colorsFont.mainColor : theme.colorsFont.secondColor}
    `}
`