import { Heading } from ".";

export default {
    title: 'Heading',
    component: Heading,

    args: {
        children: 'Fundo Branco e Texto Azul.',
        ligth: false,
    },
    argTypes: {
        children: { type: 'string' },
        ligth: { type: 'boolean' },
    },
    // Configuração de estilo no próprio software do Stotybook  
    parameters: {
        backgrounds: {
            default: 'dark'
        },
    },
};

export const Dark = (args) => <Heading {...args} ligth={true} />
Dark.args = {
    children: 'Fundo Azul e Texto Branco',
    ligth: true
}

export const Ligth = (args) => <Heading {...args} />
Ligth.parameters = {
    backgrounds: {
        default: 'ligth',
    }
}