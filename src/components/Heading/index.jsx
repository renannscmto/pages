import theme from "../../styles/theme.js"
import * as Styled from "./styles"
import P from 'prop-types'

export const Heading = ({ children, ligth = false }) => {
    return <Styled.Title theme={theme} ligth={ligth}> {children} </Styled.Title>
}

Heading.propTypes = {
    children: P.node.isRequired,
    ligth: P.bool.isRequired
}